<?php
/*
Plugin Name: Events Calendar Pro - Events List
Plugin URI: http://www.kadimi.com/
Description: -
Version: 1.0.0
Author: Nabil Kadimi
Author URI: http://kadimi.com
License: GPL2
*/

// Include Skelet (written by Nabil Kadimi)
include dirname( __FILE__ ) . '/skelet/skelet.php';

// Include options definitions
skelet_dir( dirname( __FILE__ ) . '/data' );

// Add ECP_Events_Widget widget
add_action( 'widgets_init', 'register_ecp_events_widget' );
function register_ecp_events_widget() { register_widget( 'ECP_Events_Widget' ); }
class ECP_Events_Widget extends WP_Widget {

	/**
	 * Register widget with WordPress.
	 */
	function __construct() {
		parent::__construct(
			'ecp_events_widget'
			, __( 'ECP - Events List' )
			, array( 'description' => __( 'Events Calendar Pro - Events List' ) )
		);
	}

	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
		echo $args[ 'before_widget' ];
		if ( K::get_var( 'title', $instance ) ) {
			echo $args[ 'before_title' ]
				. apply_filters( 'widget_title', $instance[ 'title' ] )
				. $args[ 'after_title' ]
			;
		}
		echo do_shortcode(
			sprintf( '[ecp_events_list show="%d" tpl="%d"]'
				, $instance[ 'show' ]
				, $instance[ 'tpl' ]
			)
		);
		echo $args[ 'after_widget' ];
	}

	/**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {

		$title = K::get_var( 'title', $instance );
		$tpl = K::get_var( 'tpl', $instance );
		$show = K::get_var( 'show', $instance );
		if ( ! $show ) {
			$show = 3;
		}

		$field = 'title';
		K::input( $this->get_field_name( $field )
			, array(
				'class' => 'widefat',
				'id' => $this->get_field_id( $field ),
				'value' => esc_attr( $$field ),
			)
			, array(
				'format' => sprintf( '<p><label>%s: :input</label></p>', __( 'Title' ) ),
			)
		);

		$field = 'tpl';
		K::select( $this->get_field_name( $field )
			, array(
				'class' => 'widefat',
				'id' => $this->get_field_id( $field ),
			)
			, array(
				'options' => array( 1 => 1, 2 => 2, 3 => 3 ),
				'format' => sprintf( '<p><label>%s: :select</label></p>', __( 'Template' ) ),
				'selected' => $$field,
			)
		);

		$field = 'show';
		K::input( $this->get_field_name( $field )
			, array(
				'class' => 'widefat',
				'id' => $this->get_field_id( $field ),
				'value' => esc_attr( $$field ),
			)
			, array(
				'format' => sprintf( '<p><label>%s: :input</label></p>', __( 'Show' ) ),
			)
		);
	}

	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {

		return $new_instance;
	}
}