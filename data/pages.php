<?php

// Register options page
paf_pages( array( 'ecp_events_lists' => array(
    'parent'       => 'edit.php?post_type=tribe_events',
    'title'        => __( 'Events Calendar Pro - Events Lists Options' ),
    'menu_title'   => __( 'Events Lists Options' ),
    'reset_button' => __( 'Repopulate with defaults' ),
) ) );