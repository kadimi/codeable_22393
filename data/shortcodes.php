<?php

// Register shortcodes
paf_shortcodes( array(
	'ecp_events_list' => array(
		'title' => __( 'Events Calendar Pro - Events List' ),
		'image' => '//png.findicons.com/files/icons/2083/go_green_web/64/list.png',
		'height' => .6,
		'width' => .6,
		'parameters' => array(
			'tpl' => array(
				'title' => 'Single event template',
				'type' => 'radio',
				'separator' => '&nbsp;&nbsp;&nbsp;&nbsp;',
				'selected' => 1,
				'options' => array(
					1 => 1,
					2 => 2,
					3 => 3,
				),
			),
			'show' => array(
				'title' => 'Number of events to show',
				'value' => 3,
			),
		),
	),
) );

function ecp_events_list( $atts ) {

	// Use defaults for "tpl" and "show"
	$atts += array(
		'tpl' => '1',
		'show' => '9999',
	);

	$tpl = htmlspecialchars_decode( paf( 'ecp_events_list_tpl_' . ($atts['tpl'] - 1 ) ) );
	$wrapper = htmlspecialchars_decode( paf( 'ecp_events_list_tpl_' . ($atts['tpl'] - 1 ) . '_wrapper' ) );
	$out = '';

	// Get All Upcoming Events
	$events = get_posts( array(
		'post_type' => 'tribe_events',
		'post_status' => 'publish',
		'posts_per_page' => - 1,
		'meta_key' => '_EventStartDate',
		'meta_compare' => '>',
		'meta_value' => date("Y-m-d H:i:s"),
		'meta_type' => 'DATETIME',
		'orderby' => 'meta_value',
		'order' => 'ASC',
	) );

	// Traverse Events 
	$shown = 0;
	foreach ( $events as $event ) {

		// Get ticket product 
		$products_ids = get_posts( array(
			'post_type' => 'product',
			'post_status' => 'publish',
			'meta_key' => '_tribe_wooticket_for_event',
			'meta_value' => $event->ID,
			'meta_compare' => '=',
			'fields' => 'ids',
			'posts_per_page' => - 1,
		) );
		if ( ! $products_ids ) { 
			continue;
		}

		// We assume one ticket product
		$product_id = $products_ids[0];


		// Get ticket stock
		$product = get_product( $product_id );
		$event->ticket_stock = $product->get_stock_quantity();
		if ( ! $event->ticket_stock ) {
			continue;
		}

		// Get event excerpt
		if ( $event->post_excerpt ) {
			$event->excerpt = apply_filters( 'the_excerpt', $event->post_excerpt );
		} else {
			preg_match( '/(?:\w+(?:\W+|$)){0,55}/', $event->post_content, $matches );
			$event->excerpt = $matches[0];
		}

		// Get event start date and time
		$event->start_date = mysql2date(
			get_option( 'date_format' ),
			get_post_meta( $event->ID, '_EventStartDate', true )
		);
		$event->start_time = mysql2date(
			get_option( 'time_format' ),
			get_post_meta( $event->ID, '_EventStartDate', true )
		);

		// Get post thumbnail
		$event->thumbnail = get_the_post_thumbnail( $event->ID, 'thumbnail' );
		$event->thumbnail_medium = get_the_post_thumbnail( $event->ID, 'medium' );
		$event->thumbnail_large = get_the_post_thumbnail( $event->ID, 'large' );
		$event->thumbnail_full = get_the_post_thumbnail( $event->ID, 'full' );

		$out .= str_replace(
			array(
				'{{event_id}}',
				'{{event_title}}',
				'{{event_url}}',
				'{{event_description}}',
				'{{event_excerpt}}',
				'{{event_thumbnail}}',
				'{{event_thumbnail_medium}}',
				'{{event_thumbnail_large}}',
				'{{event_thumbnail_full}}',
				'{{event_ticket_stock}}',
				'{{event_start_date}}',
				'{{event_start_time}}',
			),
			array(
				$event->ID,
				$event->post_title,
				get_permalink( $event->ID ),
				esc_html( $event->post_content ),
				$event->excerpt,
				$event->thumbnail,
				$event->thumbnail_medium,
				$event->thumbnail_large,
				$event->thumbnail_full,
				$event->ticket_stock,
				$event->start_date,
				$event->start_time,
			),
			$tpl
		);

		// Exit loop if required elements shown
		$shown += 1;
		if( $atts[ 'show' ] == $shown ) break;
	}

	// Wrap
	if( $wrapper ) {
		$out = str_replace( '{{event_list_items}}', $out, $wrapper );
	}

	// Return
	return $out;
}
