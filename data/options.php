<?php

// Register options page
paf_options( array(
	'ecp_events_list_tpl_0' => array(
		'page' => 'ecp_events_lists',
		'title' => __( 'Single event temlpate #1' ),
		'type' => 'textarea',
		'rows' => 5,
		'default' => trim( htmlspecialchars('
<tr class="event_list_item" id="event_list_item_{{event_id}}">
	<td>{{event_thumbnail}}</td>
	<td>
		<h2>{{event_title}}</h2>
		<h4>{{event_start_date}} at {{event_start_time}}</h4>
		<p>{{event_excerpt}}</p>
		<p class="alignright"><a href="{{event_url}}">Register Now</a></p>
	</td>
</tr>
		'
		) ),
		'description' => '<span class="description">Available replacement patterns:
			<code>{{event_id}}</code>,
			<code>{{event_title}}</code>,
			<code>{{event_url}}</code>,
			<code>{{event_description}}</code>,
			<code>{{event_excerpt}}</code>,
			<code>{{event_thumbnail}}</code>,
			<code>{{event_thumbnail_medium}}</code>,
			<code>{{event_thumbnail_large}}</code>,
			<code>{{event_thumbnail_full}}</code>,
			<code>{{event_ticket_stock}}</code>,
			<code>{{event_start_date}}</code>,
			and <code>{{event_start_time}}</code>
		</span>',
	),
	'ecp_events_list_tpl_0_wrapper' => array(
		'page' => 'ecp_events_lists',
		'title' => __( 'Wrapper for temlpate #1' ),
		'type' => 'textarea',
		'rows' => 2,
		'default' => trim( htmlspecialchars('<table class="event_list">{{event_list_items}}</table>') ),
		'description' => '<span class="description"><code>{{event_list_items}}</code> will be replaced by the events list items</span>',
	),
	'ecp_events_list_tpl_1' => array(
		'page' => 'ecp_events_lists',
		'title' => __( 'Single event temlpate #2' ),
		'type' => 'textarea',
		'rows' => 5,
	),
	'ecp_events_list_tpl_1_wrapper' => array(
		'page' => 'ecp_events_lists',
		'title' => __( 'Wrapper for temlpate #2' ),
		'type' => 'textarea',
		'rows' => 2,
	),
	'ecp_events_list_tpl_2' => array(
		'page' => 'ecp_events_lists',
		'title' => __( 'Single event temlpate #3' ),
		'type' => 'textarea',
		'rows' => 5,
	),
	'ecp_events_list_tpl_2_wrapper' => array(
		'page' => 'ecp_events_lists',
		'title' => __( 'Wrapper for temlpate #3' ),
		'type' => 'textarea',
		'rows' => 2,
	),
) );
